
import pygame
from time import sleep
from .my_class_2 import MyClass2

class MyClass():

    state = None

    def __init__(self, state=None):
        print("Initialized myclass")
        self.stringy = "stringy"
        # Initialize Pygame
        pygame.init()

        # Set the window size
        window_size = (640, 480)

        # Create the window
        self.screen = pygame.display.set_mode(window_size)

        # Set the window title
        pygame.display.set_caption('Pygame Main Loop Example uno dos')
        if state:
            self.state = state
        else:
            self.state = {"index": 0, "viesti": "Ei iloinen"}

    def get_events(self):
        return pygame.event.get()

    def update(self):
        # Clear the screen
        self.screen.fill((255, 255, 200))
        print(f"stuff {self.stringy} {self.state.get('index')}")
        sleep(1)
        self.state["index"] += 1
        #print(self.state.get("viesti"))
        foo = MyClass2()
        foo.testifunktio()


